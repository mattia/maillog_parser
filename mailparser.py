#!/usr/bin/python3

import os
import sys


# Prefer local modules over any system-installed ones to ensure that running a
# Git version from any current working directory does not have unexpected
# behaviour.
parent = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
if os.path.exists(os.path.join(parent, 'maillog_parser', 'main.py')):
    sys.path.insert(0, parent)

from maillog_parser.main import main

if __name__ == '__main__':
    main()

