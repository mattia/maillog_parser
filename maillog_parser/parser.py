#
#

import re
from dataclasses import InitVar, asdict, dataclass, field
from datetime import datetime
from typing import Dict, Optional


EMAIL_RE = (
    r"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*"
    r"@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+"
    r"[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?"
)
TIMESTAMP_RE = (
    r'^(?P<month>[A-Z][a-z]{2}) (?P<day>[0-9]{,2}) '
    r'(?P<time>[0-9]{2}:[0-9]{2}:[0-9]{2}) (?P<hostname>\w+) '
    r'postfix/[a-z]+\[[0-9]+\]: (?P<queue_id>[A-Z0-9]+): '
)

REGEX_FROM = TIMESTAMP_RE \
    + r'from=<(?P<from_address>'+EMAIL_RE+')>, ' \
    + r'size=(?P<size>[0-9]+), ' \
    + r'nrcpt=(?P<nrcpt>[0-9+]) \((?P<description>.*)\)'
PATTERN_FROM = re.compile(REGEX_FROM)

REGEX_TO = TIMESTAMP_RE \
    + r'to=<(?P<to_address>'+EMAIL_RE+')>, (?:orig_to=<.*>, )?' \
    + r'relay=(?P<relay>.*), delay=(?P<delay>[0-9.]+), ' \
    + r'delays=(?P<delays>[0-9][0-9/.]+), dsn=(?P<dsn>[0-9].[0-9].[0-9]), ' \
    + r'status=(?P<status>(sent|deferred|bounced)) \((?P<description>.*)\)'
PATTERN_TO = re.compile(REGEX_TO)

REGEX_MSGID = TIMESTAMP_RE \
    + r'message-id=<(?P<message_id>'+EMAIL_RE+')>'
PATTERN_MSGID = re.compile(REGEX_MSGID)


ParseResultType = Dict[str, str]


class ParserError(Exception):
    pass


def parse(target: str) -> Optional[ParseResultType]:
    """Parse postfix maillog including send status

    Args:
        target (str): maillog

    Returns:
        Optional[ParseResultType]: return the following dict if match

        {
            'month': 'Aug',
            'day': '1',
            'timestamp': '2022-09-26T12:01:25',
            'queue_id': '677RGS0',
            'from_address': 'dummy@gmail.com',
            'nrcpt': '1',
            'to_address': 'dummy@gmail.com',
            'relay': 'local',
            'delay': '0.06',
            'delays': '0.06/0.01/0/0',
            'dsn': '2.0.0',
            'status': 'sent',
            'description': 'delivered to maildir'
        }
    """

    for pattern in (PATTERN_FROM, PATTERN_TO, PATTERN_MSGID):
        match_obj = re.search(pattern, target)
        if match_obj is not None:
            break
    else:
        return None

    result = match_obj.groupdict()
    return ParseResult(**result)


@dataclass
class ParseResult:
    month: InitVar[str]
    day: InitVar[str]
    time: InitVar[str]

    hostname: str
    queue_id: str

    message_id: Optional[str] = None
    from_address: Optional[str] = None
    size: Optional[str] = None
    nrcpt: Optional[str] = None
    to_address: Optional[str] = None
    relay: Optional[str] = None
    delay: Optional[str] = None
    delays: Optional[str] = None
    dsn: Optional[str] = None
    status: Optional[str] = None
    description: Optional[str] = None

    timestamp: str = field(init=False)

    def __post_init__(self, month: str, day: str, time: str) -> None:
        self.timestamp = self.convert2dateime(month, day, time)

        if not (self.from_address or self.to_address or self.message_id):
            raise ParserError("Neither To nor From nor Message-ID found")

    def to_dict(self) -> ParseResultType:
        return asdict(self)

    @staticmethod
    def convert2dateime(month: str, day: str, time: str) -> str:
        tmp = datetime.strptime(f'{month}{day}{time}', '%b%d%H:%M:%S')
        tmp = tmp.replace(year=datetime.now().year)
        return tmp.strftime('%Y-%m-%dT%H:%M:%S')
