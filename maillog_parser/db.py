#

import sys
import sqlite3
import logging


DB_PATH = '/tmp/tmp/maillog.db'
DB_SCHEMA = [
    """
        CREATE TABLE maillog (
            id INTEGER PRIMARY KEY,
            timestamp INTEGER NOT NULL,
            hostname TEXT NOT NULL,
            queue_id TEXT NOT NULL,
            message_id TEXT,
            from_address TEXT,
            size INTEGER,
            nrcpt INTEGER,
            to_address TEXT,
            delay FLOAT,
            delays TEXT,
            dsn TEXT,
            status TEXT,
            description TEXT,
            UNIQUE (timestamp,queue_id,message_id) ON CONFLICT IGNORE,
            UNIQUE (timestamp,queue_id,from_address) ON CONFLICT IGNORE,
            UNIQUE (timestamp,queue_id,to_address) ON CONFLICT IGNORE
        )
    """,
    """
        CREATE INDEX maillog_queueid
        ON maillog(queue_id)
    """,
    """
        CREATE INDEX maillog_msgid
        ON maillog(message_id)
    """,
]


class Database:
    def __init__(self):
        self._conn = sqlite3.connect(str(DB_PATH))
    #    if args.verbose:
    #        self._conn.set_trace_callback(log.debug)

    def __del__(self):
        # just to be sure, do one last commit before exiting
        if self._conn:
            self._conn.commit()

    def create(self):
        with self._conn:
            cur = self._conn.cursor()
            # connection object can be used as context managers that
            # automatically commit or rollback transactions.
            for schema in DB_SCHEMA:
                cur.execute(schema)

    def execute(self, query, *args, **kwargs):
        """Excutes a raw SQL query. Returns the bare result object."""
        logging.debug(query)
        if "commit" in kwargs:
            commit = kwargs.pop("commit")
        else:
            commit = True
        result = self._conn.execute(query, *args, **kwargs)
        if commit:
            self._conn.commit()
        return result

    def exists(self):
        try:
            self.execute('SELECT MAX(_ROWID_) FROM maillog LIMIT 1')
        except Exception:
            logging.error('database is not ready')
            sys.exit(1)
