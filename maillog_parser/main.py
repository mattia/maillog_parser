#

import sys
import logging
import argparse

from .db import Database
from .importer import Importer


log = logging.getLogger(__name__)
log_stream_handler = logging.StreamHandler()
log_stream_handler.setFormatter(
    logging.Formatter(
        "[%(asctime)s] %(levelname)s: %(message)s", datefmt="%Y-%m-%d %H:%M:%S"
    )
)
log.addHandler(log_stream_handler)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--verbose", dest="verbose", action="store_true")
    parser.add_argument("--dry-run", dest="dry_run", action="store_true")
    subparsers = parser.add_subparsers(title="subcommands", dest="command")
    subparsers.add_parser("init")

    import_parser = subparsers.add_parser("import")
    import_parser_group = import_parser.add_mutually_exclusive_group(
        required=True
    )
    import_parser_group.add_argument("--path", "-p", dest="log_path")
    import_parser_group.add_argument(
        "--auto",
        dest="import_autodiscovery",
        action="store_true",
        help="try to guess the disk to import by parsing /proc/mounts",
    )
    args = parser.parse_args()

    if args.verbose:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)
    print(args)

    if args.command == "init":
        if not args.dry_run:
            Database(args).create()
        else:
            log.error("Can't call init with --dry-run.")
            sys.exit(1)
    elif args.command == "import":
        Importer(args).run()
