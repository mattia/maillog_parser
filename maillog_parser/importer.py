#
#

import logging

from .db import Database
from .parser import parse, ParserError


log = logging.getLogger(__name__)


class Importer:
    def __init__(self, args):
        self._filename = args.log_path
        print(f"Processing {self._filename}")

    def save_to_db(self, logline):
        db = Database()
        query = (
            "INSERT INTO maillog "
            "(timestamp, hostname, queue_id, message_id, from_address, size, "
            "nrcpt, to_address, delay, delays, dsn, status, description) "
            "VALUES (unixepoch(:timestamp), :hostname, :queue_id, :message_id,"
            " :from_address, :size, :nrcpt, :to_address, :delay, :delays, "
            ":dsn, :status, :description)"
        )
        db.execute(query, logline.to_dict())

    def run(self):
        with open(self._filename) as f:
            data = f.readlines()

        for line in data:
            try:
                result = parse(line)
            except ParserError:
                log.warning(f"failed to parse line: {line}")
            if result is None:
                continue

            self.save_to_db(result)
